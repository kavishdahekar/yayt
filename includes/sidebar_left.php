            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h4>Yet Another<br>Hybris Tutorial</h4>
                </div>

                <ul class="list-unstyled components">
                    <li>
                        <a href="/?page=intro">Welcome Page</a>
                    </li>
                    <li>
                        <a href="/?page=install">Installing Hybris</a>
                    </li>
                    <li>
                        <a href="/?page=intellij">IntelliJ - your all-in-one swiss knife</a>
                    </li>
                    <li>
                        <a href="/?page=ant">Ant - the bug you want to keep</a>
                    </li>
                    <li>
                        <a href="/?page=extensions">Extensions</a>
                    </li>
                    <li>
                        <a href="/?page=items">Hybris Itemtypes</a>
                    </li>
                    <li>
                        <a href="/?page=impex">Importing and Exporting in Hybris</a>
                    </li>
                    <li>
                        <a href="#">Displaying a product on our page</a>
                    </li>
                    <li>
                        <a href="#">Modifying a product - add an attribute</a>
                    </li>
                    <li>
                        <a href="#">WCMS -  in depth</a>
                    </li>
                    <li>
                        <a href="#">SOLR - search + facet</a>
                    </li>
                    <li>
                        <a href="#">Want to learn more?</a>
                    </li>
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a></li>
                    <li><a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a></li>
                </ul>
            </nav>