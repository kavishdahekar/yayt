<h2>Importing and Exporting in Hybris</h2>

<p>Now that we know how items and their attributes are defined and generated, lets take a look at how we go about entering data into the database.</p>

<p>Hybris uses a custom csv based format called "impex" which is a short form for "Import-Export".</p>
<p>Impex files are nothing but text files with a .impex extension.</p>

<p>In essense, an impex file will contain a header followed by data lines. The header defines which item the data is being added to and what attributes of the item the data is being supplied for. The following data lines contain the data for the item and its attributes in the sameoreder as that mentioned in the header.</p>

<p>Lets understand this with an example. Recall the item "Product" from the previous chapter and its attributes name, description, unit, thumbnail and picture. Below is a sample impex code that we could use for adding a product with name and description to the database.</p>

<pre><code>
$lang=en
$catalogVersion=catalogVersion(catalog(id[default='apparelProductCatalog']),version[default='Staged'])[unique=true,default='apparelProductCatalog:Staged']

INSERT_UPDATE Product;code[unique=true];$catalogVersion;name[lang=$lang];description[lang=$lang];
;my_product_code;;My Product;My product's description
</code></pre>

<p>Macros are defined with a preceding $ such as $lang. While importing the impex code, the import engine will replace all occurences of $lang with its value that is en in this case. The two macros defined in the above code are $lang and $catalogVersion. Recall that a Product belongs to a ProductCatalogVersion hence when creating a product we need to add it to a specific product catalog. In our case, the b2c accelerator has already created a product catalog named "apparelProductCatalog".</p>

<p>The header line starting with INSERT_UPDATE indicates that the data lines following the header are to be added to the database if not already present and updated if already present in the database. Other possible values here could be INSERT, UPDATE and DELETE. The attribute "code" which was defined in core-items.xml (see previous chapter) is also given a modifier unique=true to ensure that all data entries will have a unique value for code. The catalogVersion is defined as a macro and points to the apparelProductCatalog. The attribute name is given a modifier lang to indicate the language for which we want to enter the name. Recall that the attribute name was defined as a localized string to allow us to enter different values for different languages. Similar to name, description also expects us to indicate the language.</p>

<p>All lines below a header are considered data lines until another header line is encountered. The data for each attribute is separated by a semi-colon. For attributes that already have a default value provided for them in the header, the data can be left empty as shown in the above example for the data value of catalogVersion.</p>

<p>To get a better understanding ofhow impexes work lets check the apparel store's products. If you recall, the trainincore-items.xml defines ApparelProduct item that extends itemtype Product. Lets see how the sample data for the apparel site is entered into ApparelProduct. Switch to IntelliJ and on the left side you should see the project tree. If you don't see it then click the "View" button in the title menu bar and select "Tool Windows" -> "Project". At its top level, you will see the project divided into multiple modules namely "Custom", "Hybris" and "Platform". Under the "Hybris" module locate the ext-data folder and under it locate the apparelstore extension. This is where all the impex files for the apparel store have been placed. Under the apparelstore locate the following file "apparelstore/resources/apparelstore/import/sampledata/productCatalogs/apparelProductCatalog/products.impex". You could alsosimple press Ctrl+Shift+N for quick search and search for "apparelstore/resources/apparelstore/import/sampledata/productCatalogs/apparelProductCatalog/products.impex" to locate the file. Open the products.impex file and you will see quite a few macros defined at the top of the file such as $productCatalog, $taxGroup, etc which are not important to be understood right now.</p>

<p>Below the macros you will see the first INSERT_UPDATE for inserting data into ApparelProduct itemtype. The attributes being added are code,catalogVersion,unit,supercategories,varianttype,approved,taxGroup,ean and genders. The attribute genders was added to ApparelProduct in the trainingcore-items.xml file. All the other attributes are inherited form the parent type Product. Notice how ead data item is added on a separate line below the header line. Each attribute is separated by a semi-colon and some attributes are simply skipped by using two consecutive semi-colons. One greate feature of IntelliJ is when you plaveyour cursor on any of the data attributes, the corresponsing attribute in the header is highlighted.</p>

<p>Now that we know how products are added to the database via impex files, lets try to find a product from the apparel site and then try to locate it in the impex files. Head over to your browser and open your apparel site https://apparel-uk.local:9002/trainingstorefront/en/</p>

<p>In the header meanu bar visit the page "STREETWEAR" -> "MEN" -> "T-SHIRTS" or simply click here(https://apparel-uk.local:9002/trainingstorefront/en/Categories/Streetwear-men/T-Shirts-men/c/250100). This page is called a PLP or Product Listing Page.</p>

<p>Look for the "System Tee SS lime XL" and click on it (or simply click here : https://apparel-uk.local:9002/trainingstorefront/en/Categories/Streetwear-men/T-Shirts-men/System-Tee-SS/p/300737283). This page is called a PDP or Product Details Page.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/impex/tshirt_page.png" height="500">

<p>Let's try to find where this T-shirt has been defined in the impex files. Copy the name of the Tshirt "System Tee SS lime XL" and head over to IntelliJ. Press Ctrl+Shift+F forsearching in all files. Since we will only be looking in impex files, check the "File mask" option and enter "*.impex" in its adjacent textbox to indicate that you want IntelliJto search only in files whose names end in ".impex". The results should show you a single file namely products_en.impex where the English name of the T-shirt "System Tee SS lime XL" was defined.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/impex/search_in_all_impex.png" height="500">

<p>We can see that the code of this product is "300737283". We can use this code to find which other impex files had data related to this product. Press Ctrl+Shift+F again and search for "300737283" in all impex files. The search results will show you that the product has been mentioned in MANY impex files for describing everything regarding the product from its name, price, stocklevel, tax, etc. </p>

<p>Let us see how we would go about editing the name of this product in the database using impexes.</p>
<p>The current name of this Tshirt is "System Tee SS lime XL". Let us change it to "Updated System Tee SS lime XL". Headover to the products_en.impex file where the name of this Tshirt was defined. Scroll all the way up and copy the "UPDATE" header of the impex as well as all the macros defined at the top. Also copy the line which defined the name of our Tshirt and put it below the header line. here they are below for your reference:</p>

<pre>
<code>$lang=en
$catalogVersion=catalogVersion(catalog(id[default='apparelProductCatalog']),version[default='Staged'])[unique=true,default='apparelProductCatalog:Staged']


UPDATE Product;code[unique=true];$catalogVersion;name[lang=$lang];summary[lang=$lang];description[lang=$lang];ApparelStyleVariantProduct.style[lang=$lang];ApparelSizeVariantProduct.size[lang=$lang]
;300737283;;System Tee SS lime XL;;;;XL;;</code></pre>


<p>Now update the name of the Tshirt name. The resulting impex should look like this:</p>

<pre>
<code>$lang=en
$catalogVersion=catalogVersion(catalog(id[default='apparelProductCatalog']),version[default='Staged'])[unique=true,default='apparelProductCatalog:Staged']


UPDATE Product;code[unique=true];$catalogVersion;name[lang=$lang];summary[lang=$lang];description[lang=$lang];ApparelStyleVariantProduct.style[lang=$lang];ApparelSizeVariantProduct.size[lang=$lang]
;300737283;;Updated System Tee SS lime XL;;;;XL;;
</code>
</pre>

<p>That's it, time to import this impex code into our database. Hybris gives us a lot of handy ways for doing this. Lets use the way you will mostly likely always be using - the HAC (Hybris Administration Console). We already used HAC while initializing our system in chapter 5. Lets open up hac again in the browser : https://localhost:9002/hac</p>

<p>Login using the credentials admin : nimda.</p>

<p>Navigate to "Console" -> "Impex Import" (or click here : https://localhost:9002/console/impex/import)</p>

<p>You will land on the ImpEx Import page with a large textbox.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/impex/impex_import.png" height="500">

<p>Paste the above modified impex code in the text box. Click on the blue "Validate Content" button and ensure you get a "Import script is valid success message". Click on "Import content" button and ensure you get the "Import finished successfully" message.</p>

<p>However, we need to do one more thing, before our change is visible on our Tshirt's product details page. You can visit the page (https://apparel-uk.local:9002/trainingstorefront/en/Categories/Streetwear-men/T-Shirts-men/System-Tee-SS/p/300737283) and see that the name of the product is still "System Tee SS lime XL". If you notice the impex we just imported, the catalogVersion's version is set to default to the value "Staged". Every catalog that is created in Hybris is maintained in two versions, one staged and one online. All changes made are first made to the "staged" catalog to ensure that the system is stable even after the changes. If everything seems fine, the staged catalog is then "synchronized" with the online catalog. The synchronization process involves nothing but copying the changes over from the staged catalog version to the online catalog version.</p>

<p>So now that we know that our changes were successfully imported into the staged version of apparelProductCatalog, how do we synchronize the catalog and move these changes to the online version? This is where we will make use of another backend module provided by hybris named the "Product Cockpit".</p>

<p>Head over to your browser and navigate to the product cockpit's url : <a href="https://localhost:9002/productcockpit" target="_blank">https://localhost:9002/productcockpit</a></p>

<p>Although the login form will be prefilled with details for productmanager, login using the admin credentials - admin : nimda</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/impex/prodcockpit_login.png" height="500">

<p>Once logged in, you will see the "Apparel Product Catalog Staged" and "Apparel Product Catalog Online" versions in the left sidebar. Click on the "Apparel Product Catalog Staged" entry and the main section of the page will load a list of all products present in the catalog. Youwill notice that one of these entries has a red disjoint circle icon inidicating that this product has been modified but has not yet been synced to the online version. Instead of syncing this product alone, lets synchrnoze the entire staged catalog. Right click on the "Apparel Product Catalog Staged" entry in the left sidebar and a context-menu should open up. Click on the "Sync selected versions" option and wait till you get a popuover message stating "Synchronization of Apparel Product Catalog Staged has finished". Notice that the red disjoint circle present over the Tshirt we had modified has changed into a green circle.</p>

<p>Now head over to the Tshirt's PDP (<a href="https://apparel-uk.local:9002/trainingstorefront/en/Categories/Streetwear-men/T-Shirts-men/System-Tee-SS/p/300737283" target="_blank">https://apparel-uk.local:9002/trainingstorefront/en/Categories/Streetwear-men/T-Shirts-men/System-Tee-SS/p/300737283</a>) and you'll see that its name has changed to "Updated System Tee SS lime XL".</p>

<p>If you don;t like impexes, you can usethe product cockpit to add,remove and modify products from the product catalog. Always remember, your changes will first go in the staged catalog version which you will then synchronize to the online one. </p>

<p>Play around the product cockpit and see if you can add a new product to the catalog and get it to show up on the front-end. Here's an article that explains the steps for doing that : <a href="https://help.hybris.com/6.7.0/hcd/8b2e56fb866910149bf9a32053fd55ba.html" target="_blank">https://help.hybris.com/6.7.0/hcd/8b2e56fb866910149bf9a32053fd55ba.html</a></p>