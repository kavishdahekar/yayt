<h2>Welcome to <a href="https://en.wikipedia.org/wiki/Yet_another" target="_blank">Yet Another</a> Hybris Tutorial.</h2>

<p>A quick google search and you will see the internet is full of tutorials and explanations on how to get Hybris up and running. What is so special with this tutorial, you might ask? Let us go through a quick list of the goals and objectives of this document:</p>

<ul>
	<li>The primary purpose of this document is to help a developer, who has never worked on Hybris, become a productive Hybris developer as quickly as possible.</li>
	<li>The document specifically tackles handpicked topics that a Hybris developer would frequently encounter in a project.</li>
	<li>Hybris is extremely vast, this guide does not intend to cover everything.</li>
	<li>The tutorial assumes the user is using a windows machine. But every single step can be easily mapped to a linux machine as well.</li>
	<li>Its okay to not understand everything immediately. Give it some time.</li>
	<li><strong>y</strong> stands for Hybris.</li>
</ul>


<p>What this tutorial will help you do:</p>
<ul>
	<li>Get an overview of what Hybris is and how it works</li>
	<li>Help you learn how to find your way around while learning Hybris</li>
	<li>Teach you how to scracth your own itch</li>
</ul>

<p>It takes developers months of hands-on development in a live project to get comfortable with the vast codebase of Hybris and its nitty-gritties. This tutorial focuses on handpicked topics that a Hybris developer encounters on a daily basis and provides detailed step by step explanation on exactly how to acheive expected results.</p>

<p>Requirements from the reader (make a table, right side has meters):</p>
<ul>
	<li>Java knowledge (moderate to high)</li>
	<li>OOPS concepts (moderate to high)</li>
	<li>Spring MVC (low to moderate)</li>
	<li>Proficiency in Copy+Paste (extremely high)</li>
</ul>

<p>With that out of the way, lets dive in with a brief introduction to Hybris.</p>
*begin sritej's content<br>
<p>Hybris is an e-commerce software where it provides solution for B2C,B2B and Telco markets.Technically it is a java based platform that is predominantly built on spring framework.The core business logic offers many pre-built functionalities like cart,product and content management,login/signup,etc that are basic for an e-commerce site. Personalized Modules are developed by extending the core logic and customizing it accordingly.</p>
*end sritej's content<br>


<h2>Collapsible Sidebar Using Bootstrap 3</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

    <div class="line"></div>
    <pre class="command-line language-powershell" data-prompt="C:\hybris\yaYt>" data-output="2-19">
        <code class=" language-powershell">
ant all && hybrisserver.bat debug
        </code>
    </pre>
    <div class="line"></div>

    <pre>
        <code class="language-java">
/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package org.training.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Controller for home page
 */
@Controller
@RequestMapping("/")
public class HomePageController extends AbstractPageController
{
    private static final String LOGOUT = "logout";
    private static final String ACCOUNT_CONFIRMATION_SIGNOUT_TITLE = "account.confirmation.signout.title";
    private static final String ACCOUNT_CONFIRMATION_CLOSE_TITLE = "account.confirmation.close.title";

    @RequestMapping(method = RequestMethod.GET)
    public String home(@RequestParam(value = WebConstants.CLOSE_ACCOUNT, defaultValue = "false") final boolean closeAcc,
            @RequestParam(value = LOGOUT, defaultValue = "false") final boolean logout, final Model model,
            final RedirectAttributes redirectModel) throws CMSItemNotFoundException
    {
        if (logout)
        {
            String message = ACCOUNT_CONFIRMATION_SIGNOUT_TITLE;
            if (closeAcc)
            {
                message = ACCOUNT_CONFIRMATION_CLOSE_TITLE;
            }
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, message);
            return REDIRECT_PREFIX + ROOT;
        }

        storeCmsPageInModel(model, getContentPageForLabelOrId(null));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
        updatePageTitle(model, getContentPageForLabelOrId(null));

        return getViewForPage(model);
    }

    protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
    {
        storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
    }
}
        </code>
    </pre>

    <div class="line"></div>

    <h2>Lorem Ipsum Dolor</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

    <div class="line"></div>

    <h2>Lorem Ipsum Dolor</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

    <div class="line"></div>

    <h3>Lorem Ipsum Dolor</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>