<h2>IntelliJ : Your all-in-one Swiss Knife</h2>

<p>If you are new to Java, you will soon learn that only Chuck Norris can write java code without an IDE. For all us mortals, and IDE is not only a tool for manging files and assisting codewriting but is an absolute necessity.</p>

<p>Although many developers prefer Eclipse and while it works quite well for all intended purposes, the IDE of our choice is IntelliJ. If you have used both, you will agree with us when we say IntelliJ has a much more intuitive UX, looks and performs better and has dedicated hybris plugins that makea lot of things easier for the developer. If you need futher convincing, checkout this stackoverflow thread for details : <a href="https://softwareengineering.stackexchange.com/a/24231" target="_blank">https://softwareengineering.stackexchange.com/a/24231</a> .</p>

<p>You can get the free community edition of Eclipse from their official site here  : <a href="https://www.jetbrains.com/idea/" target="_blank">https://www.jetbrains.com/idea/</a>. The free version is more than enough for our development scope, but in case you can manage to grab the paid Ultimate version then go for it.</p>

<p>Once downloaded and installed when you launch IntelliJ for the first time you will be greeted with this screen:</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/first_launch.png">

<p>First and foremost, we will install all the plugins we will be requiring for our dev workflow. At the bottom right of the dialog box, click on the "Configure"->"Plugins" button. This should open the plugins dialog boxas shown below.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/plugins.png">

<p>Click on the "Browse Repositories" button and the search field on the top search for "Hybris Integration". In the search results click on the "Hybris Integration" plugin and in the right pane click on "install".</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/hybris_integration.png">

<p>You can explore the vast array of plugins offered for IntelliJ that can do everything from automate dev routines to emulate VIM key input style for the console lovers. Once you have installed the plugins IntelliJ will ask you for you to restart it. Restart it and you will be shown the same screen we began with before.</p>

<p>Hybris in itself comes bundled as a set of java projects that can be easily identified and imported by Java IDEs. Click on the "Import Project" button. Using the directory tree displayed, navigate to the folder where we extracted and installed your hybris files, namely : "C:\hybris\yayt". Select the "yayt" directory and click next.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/import_folder.png">

<p>From the project type options available select "Hybris".</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/project_type_hybris.png">

<p>TIP : If you don't see the "Hybris" project type, it means the Hybris Integration Plugin has not been properly installed.</p>

<p>Click next and ignore any warning about JavaEE features not being available in Community edition.</p>

<p>Leave all options as is on this screen and click next and your hybris project should begin importing.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/import_settings.png">

<p>You will be shown a dialog will all hybris extensions in a list with the ones being imported with a checkmark. Leave everything as it is.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/import_extensions.png">

<p>At the end of the process if you are asked to select a hybris project to import, just leave everything unchecked.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/import_last_screen.png">

<p>Lastly you will asked to point IntelliJ to your java SDK. Click the green "+" button and navigate to the folder where you installed your JDK in chapter 2. The dialog box should show you the jar files it has loaded from the jdk below.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/import_jdk.png">

<p>Click the "Finish" button. Once the project import is done, click on "View"-> "Tool Windows" -> "Project" to view the source directories and files of your projects.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/intellij/imported_final.png">

<p>In order to maintain the scalability of the code, Hybris has been designed to exist in seperate logical extensions that work together. We will learn more about extensions in the sub-sequent chapters. Each extension is a java project in itself which can be imported into an IDE. IntelliJ imports all the extensions together and creates a parent-project containing all these child projects. Hence the "Project" window in IntelliJ will show you multiple projects such as platform, catalog, storefront, etc. These projects will be futher grouped under modules such as "Hybris" (contains the accelerator extensions), "Platform" (Contains the core hybris extensions) and "Custom" (Contains extensions created and added by you).</p>

<p>We will end this chapter for now but the rest of the chapters of the tutorial will keep educating you about the various features of IntelliJ as well as all the handy shortcuts you can use for navigating around the code.</p>