<h2>Ant - The BUG You Want To Keep</h2>

<p>Owing to the complex project structure of hybris, inteconnected depencies and a long list of tasks to be performed for a complete build it makes sense to automate the entire process intot a single command.</p>

<p>Enter Ant!</p>

<p>At its core Ant is a build tool created by our friendly neighborhood the Apache Software Foundation. Hybris extensively uses ant for automatic various tasks such as building, update, initialize, server start and stop among others.</p>
<p>Hybris is shipped with standalone ant binaries and you need not install ant yourself. </p>
<p>If you want to see all the possible ant targets you can run on your hybris project navigate to "C:\hybris\yayt\hybris\bin\platform" and run the following commands:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>">
<code class="language-powershell">setantenv.bat</code>
</pre>

<div class="alert alert-info">
TIP : everytime you open a console, you must run the setantenv.bat file in order to set the appropriate ant paths and environment variables in the console's session so you can use the ant command correctly.
</div>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>">
<code class="language-powershell">ant -p</code>
</pre>

<p>This will return a list of ant targets you can execute along with their explanations. Below is a small list of the mostly used ant commands which we will encounter:</p>

<pre>
all                                        Builds all extensions in your hybris project and restarts the server if already running.
addoninstall                               Installs addon (more on addons later)
build                                      Builds all extensions
clean                                      Cleans platform and all extensions
extgen                                     Used for creating extensions 
importImpex                                Import ImpEx from file. (more on impexes later)
initialize                                 Performs initialization (we already performed an initialization using a different command in chapter 2)
modulegen                                  Runs modulegen (more on modules later)
updateMavenDependencies                    Updates all maven dependencies in all extensions
updatesystem                               Performs system update explained in chapter 2.
</pre>

<p>This is the most we need to know about ant for now. As and when we execute a new ant command we will learn its syntax and purpose along the way.</p>