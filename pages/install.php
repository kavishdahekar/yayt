<h2>Installing Hybris</h2>

<p>Due to the high-level of complexity an e-commerce application can reach, it will be a rare scenario where you will develop a hybris based e-commerce application from scratch. Hybris developers understood that almost all e-commerce applications can be roughly divinded into a few coarse-grained categories and created "accelerators" as an aid for developers to jump start their projects.</p>

<p>More info on accelerators can be found here : <a href="https://www.tutorialspoint.com/sap_hybris/sap_hybris_accelerators_concept.htm">https://www.tutorialspoint.com/sap_hybris/sap_hybris_accelerators_concept.htm</a></p>

<p>Hybris accelerators are nothing but fuly functioning sample projects that contain everything from sample data, well-designed mobile-friendly storefront, search, cart, checkout and many other features essential to an e-commerce site. It would not be wrong to say that an accelerator project could be deployed as a fully functioning e-commerce site with very few modifications. But as with the internet, customization is key. And this is where your role as a Hybris developer comes in. Every hybris project will carefully choose an accelrator best suited for the project's requirements. Then the project teams modifies the accelerator - both frontend as well as backend - to achieve the desired look and functionalty expressed by the customer.</p>

<p>Let's jump in and see for ourselves what exactly an accelerator looks like.</p>

<h3>INSTALLATION</h3>

<p>First, we must procure the Hybris zipped archive that contains everything we will need for working with hybris including a build tool(ant), a portable database(hsqldb) as well as an embedded server (apache tomcat).</p>

<p>For the purpose of this tutorial, we will be using the <a href="https://help.hybris.com/6.7.0/hcd/8c39dca2866910148073b1b86eeec916.html" target="_blank">latest release 6.7</a> which includes many key enhancements to the hybris core functionalities.</p>

<p>Hybris being Java Spring based software, requires Java to be installed on your system. Anything >= JDK 8 should suffice for both learning as well as development purposes. This tutorial assumes you know how to do that including setting the JAVA_HOME environment variable. If not, here are some links explaining the steps for doing that in windows : <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/install/windows_jdk_install.html#CHDEBCCJ">Windows JDK Installation</a>,<a href="https://confluence.atlassian.com/doc/setting-the-java_home-variable-in-windows-8895.html">Setting JAVA_HOME env variable</a>. Linux guys, we trust you can figure this out yourselves ;).</p>

<p>Time to extract the Hybris zip folder. If you have any prior experience with Java, you would know how lengthy Java class names can get. (How's this for an example : SimpleBeanFactoryAwareAspectInstanceFactory.class). Windows on the other hand has a 260 character limit on file paths (<a href="https://stackoverflow.com/questions/1880321/why-does-the-260-character-path-length-limit-exist-in-windows?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa" target="_blank">windows file path limit</a>. Linux can handle much longer paths). Putting two and two together, this means you need to extract your hybris zip archive as close to your drive's root as possible. This is what we would suggest</p>

<pre>C:\hybris\yayt\extract_your_files_here</i></pre>

<div class="alert alert-info">
TIP : Owing to long java class names and windows file path length limit, keep your hybris projects as close to the root as possible.
</div>

<div class="alert alert-info">
TIP : Hybris zip archives are large (close to 2GB). Start the extraction and go grab a coffee, its gonna take a while. On a quad core windows machine with 16GBs of RAM, 7ZIP archiver takes about 20 minutes to complete the extraction.
</div>

<p>Once you have finished extracting your hybris archive, your folder structure should look something like this:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt>" data-output="2-16">
<code class="language-powershell">dir
 Directory of C:\hybris\yayt
04/25/2018  07:10 PM    &lt; DIR &gt;       .
04/25/2018  07:10 PM    &lt; DIR &gt;          ..
03/17/2018  09:56 AM    &lt; DIR &gt;          build-tools
02/19/2018  05:27 AM    &lt; DIR &gt;          c4c-integration
04/24/2018  01:08 PM    &lt; DIR &gt;          hybris
03/17/2018  09:56 AM    &lt; DIR &gt;          hybris-ems
03/07/2018  04:57 AM    &lt; DIR &gt;          hybris-Mobile-Apps-SDK
03/17/2018  09:56 AM    &lt; DIR &gt;          hybris-sbg
04/28/2018  02:31 PM    &lt; DIR &gt;          idea-module-files
04/24/2018  01:08 PM    &lt; DIR &gt;          installer
03/17/2018  09:56 AM    &lt; DIR &gt;          licenses
03/17/2018  09:56 AM               229 README
               1 File(s)            229 bytes
              12 Dir(s)  xyz,123,456,789 bytes free</code>
</pre>


<p>Exploring the hybris directory structure would be out-of-scope for this tutorial. But we will certainly take this opporunity to peek inside the "installer/recipes" folder.</p>

<p>The recipes folder contains scripts for installing all available hybris accelerators. You will see that hybris offers a variety of accelerators from the common b2c flavor all the way to b2b2c acclerators as well. In order to start off light, we will choose the b2c accelerator whose install script can be found in the "installer/recipes/b2c_acc" folder.</p>

<p>Let's install our accelerator. Take a look into the C:\hybris\yayt\hybris folder and notice its contents. We will check this folder again after the recipe installation to see the new files that were created.</p>
<p>Open up a command prompt, navigate to the installer folder and run the following command.</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\installer>">
<code class="language-powershell">install.bat -r b2c_acc</code>
</pre>

<div class="alert alert-info">
TIP : Although maven and gradle are the preferred build tools for the newer hybris releases, this tutorial primarily uses ant which works great for all intended purposes.
</div>

<p>The accelerator recipe installation should end with a "BUILD SUCCESSFUL" message. This will create+copy the appropriate files and directory structure required by the accelerator. You can see them in the C:\hybris\yayt\hybris directory.</p>

<p>Once the recipe installation is complete, we will need to initialize the system. Think of initialization as a way of cleaning the database, creating all tables and relations from scratch and importing all required data into the database. In a live project you would typically initialize the system only once during the deployment of the project. Any modifications to the project after that must be carefully monitored so as to avoid the requirement of another initialization. All such subsequent modifications will be taken care of by "updating" the system that works similar to initialization except that it does not clean your database. More info <a href="https://stackoverflow.com/questions/29936212/what-is-the-difference-between-system-initialization-and-update-in-hybris?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa" target="_blank">here</a>.</p>

<p>Hybris uses a portable in-memory database HSQLDB for development purposes. All deployments will switch out HSQLDB for more stable commercial grade database systems. If you want to see exactly where the hsqldb file is present, you can find it here : C:\hybris\yayt\hybris\data\hsqldb\mydb.data . If you're feeling a bit freaky you can even try to open this file and take a look inside using the already provided hsqldbjar by running the following command:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt>">
<code class="language-powershell">java -cp C:\hybris\yayt\hybris\bin\platform\lib\dbdriver\hsqldb-2.3.4.jar org.hsqldb.util.DatabaseManager --url jdbc:hsqldb:file:C:\hybris\yayt\hybris\data\hsqldb\mydb</code>
</pre>

<p>Let's initialize our database. Run the following command to initialize your hybris project:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\installer>">
<code class="language-powershell">install.bat -r b2c_acc initialize</code>
</pre>

<div class="alert alert-info">
TIP : If you already took a coffee break while extracting the hybris archive, now would be a good time to break for lunch. On a modern machine, the initialization process can take anywhere from 30 minutes to an hour.
</div>

<p>The initialization should end in a "BUILD SUCCESSFULL" message. You may again check the hsqldb to note what tables and data were created during the initialization process.</p>

<p>Time to fireup our server. There are many ways to do this, including directly using the install.bat file. However we will use the more often used ant command for starting our server. Navigate to the folder "C:\hybris\yayt\hybris\bin\platform" and run the following command:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>">
<code class="language-powershell">setantenv.bat</code>
</pre>

<p>This will set the ant environment variables in your current console session which will allow you to use the "ant" command.</p>

<p>Run the following command to run the server:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>">
<code class="language-powershell">hybrisserver.bat</code>
</pre>

<div class="alert alert-info">
TIP : You can start the server in debug mode by running "hybrisserver.bat debug". This will allow you to set breakpoints and inspect the java code while the server is running.
</div>

<div class="alert alert-info">
TIP : Server starup will take another 15 to 20 minutes. Time for another cup of coffee. Might as well take a bathroon break too.
</div>

<p>Assuming you have followed all the steps mentioned till now, your server should have started successfully. You can inspect the console for a message like the one below:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>" data-output="2-7">
<code class="language-powershell">
Jan 28, 2018 4:06:29 PM org.apache.coyote.AbstractProtocol start
INFO: Starting ProtocolHandler ["https-jsse-nio-9002"]
Jan 28, 2018 4:06:29 PM org.apache.coyote.AbstractProtocol start
INFO: Starting ProtocolHandler ["ajp-nio-8009"]
Jan 28, 2018 4:06:29 PM org.apache.catalina.startup.Catalina start
INFO: Server startup in 521767 ms</code>
</pre>

<p>In case you were away form the screen for a long time you might see the console show the following messages:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>" data-output="2">
<code class="language-powershell">
INFO  [update-apparel-deIndex-cronJob::de.hybris.platform.servicelayer.internal.jalo.ServicelayerJob] (update-apparel-deIndex-cronJob) [SolrIndexerJob] Started indexer cronjob.</code>
</pre>

<p>Lets check if the server is indeed up. Head over to your favourite web browser (Chrome! Chrome is your favourite browser!) and navigate to the following url:</p>

<p><a href="https://localhost:9002/mcc">https://localhost:9002/mcc</a></p>

<p>Ignore any SSL warnings you might get and if your server has successfully started you should be seeing the "hybris commerce cockpit" login page. We will visit this page again later. But for now, lets see what all the accelerator has installed for us.</p>

<p>The b2c accelerator installs two demo e-commerce sites namely electronics and apparel. The apparel site is available in two languages apparel-uk and apparel-de. Since your machine might have multiple hybris sites installed, hybris uses the hosts file to internally map domain names to localhost so we can differentiate the different sites.</p>

<p>Open any text editing app in administrator mode and open the following file "C:\Windows\System32\drivers\etc\hosts". Add the following lines at the bottom of the file:</p>

<pre>
127.0.0.1       electronics.local
127.0.0.1       apparel-uk.local
127.0.0.1       apparel-de.local
</pre>

<p>Save the file. Now we are ready to visit the sites installed by the b2c accelerator. Visit the following url using your web browser and visit the following urls :</p>

<p><a href="https://apparel-uk.local:9002/yacceleratorstorefront/apparel-uk/en/" target="_blank">https://apparel-uk.local:9002/yacceleratorstorefront/apparel-uk/en/</a></p>
<p><a href="https://apparel-de.local:9002/yacceleratorstorefront/apparel-de/en/" target="_blank">https://apparel-de.local:9002/yacceleratorstorefront/apparel-de/en/</a></p>
<p><a href="https://electronics.local:9002/yacceleratorstorefront/electronics/en/" target="_blank">https://electronics.local:9002/yacceleratorstorefront/electronics/en/</a></p>

<p>All of these sites are almost fully functioning allowing you to browse and search for products, view their details, compare them, add them to cart, register for an account, check out using your cart and view your orders. You can see how easy this makes our work. In case a customer wants us to create a website for him to sell his custom made garments, we can start off with the apparel site and modify its looks ad functionality according to the customer's needs, remove the default products, upload the customer's products, add configuration settings for emails, sms, payment gateway, etc and viola!</p>

<p>Take some time to explore the apparel site as we will be using it in our subsequent lessons.</p>