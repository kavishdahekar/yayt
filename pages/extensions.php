<h2>Extensions</h2>

<p>Hybris has been designed from bottom up to be highly extensible. All similar functionality is grouped into a java project called an "extension". Multiple extensions inter-dependant on each other come together to form one single hybris project.</p>

<p>For example, in the case of our b2c accelerator the "storefront" or the part of our project that specifically deals with the frontend ui+ux along with all frontend functionality is all contained in a single extension whose name you might have been able to guess from the apparel and electronics sites we visited in chapter 2 - "yacceleratorstorefront".</p>

<div class="alert alert-info" role="alert">
QUOTE : An extension is an encapsulated piece of the Hybris Commerce Suite that can contain business logic, type definitions, a web application, or Hybris Management Console(hMC) configuration.
</div>

<p>The localextensions.xml file contains a list of all extensions that will be loaded into our hybris project. Since you as a reader don't know the exact location of this file, lets search and open up this file using our brand new IDE we setup in chapter 3.</p>

<p>Press Ctrl + Shift + N which will open up a quick search box which you can use for quickly locating and opening files by their filenames. Type "localextensions.xml" in the search box and the localextensions.xml file should show up in the search results.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/extensions/quick_search.png">

<p>Use your mouse or arrow keys to select the localextensions.xml file and press enter. This will open up the file and you will see a huge list of extensions that the b2c accelerator has loaded inorder to provide all the demoable functionality we saw on the electronics and apparel sites. You will see the yacceleratorstorefront, electronicsstore and apparelstore extensions listed here along with many others that contain specific functionality and play specific roles in the project.</p>

<p>Everytime you add any new feature/functionality or modify any OOTB(Out Of The Box) one, you will NEVER change any of the existing files or extensions provided by Hybris. Your custom code extending the functionality of hybris and any of its accelerators will ALWAYS go in a separate new extension. As a rule of thumb in almost all cases, if anything you are doing requires you to modify any of the OOTB files then you're doing it wrong.</p>

<h3>How do we create an extension?</h3>

<p>A new extension can be created by invoking the ant extgen command and proving a name, package, etc for your new extension. Unless you specify a custom path, newly generated extensions will be created in the custom folder present here : "C:\hybris\yayt\hybris\bin\custom".</p>

<p>Instead of creating a single extension, hybris customizations projects will mostly create a set of extensions that cover a range of functionality that can be modified. Such a set of extensions is called a module and can be generated using the ant modulegen command.</p>
<p>Let's invoke modulegen and create some empty extensions using OOTB provided templates.</p>

<p>If your hybris server is still running, stop it by pressing Ctrl+C and pressing "y" to confirm you want to stop it.</p>

<p>Navigate to "C:\hybris\yayt\hybris\bin\platform" and run the following commands:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>" data-output="2,3">
<code class="language-powershell">setantenv.bat
Setting ant home to: C:\hybris\yayt\hybris\bin\platform\apache-ant-1.9.1
Apache Ant(TM) version 1.9.1 compiled on May 15 2013
ant modulegen</code>
</pre>

<p>The modulegen tool will ask you to enter values for the names of the extensions, package, etc. You can press enter at every step to let modulegen use default values. The default values will use the base name "training" for all the extensions being created. The process will take a few seconds and once completed modulegen would have created exactly 7 extensions in the "C:\hybris\yayt\hybris\bin\custom" folder namely:</p>

<pre>
trainingcockpits			: To be used for extending hybris backend cockpits
trainingcore				: To be used for extending the core type-system and defining new types (More on type system later)
trainingfacades				: To be used for adding facade layers classes following the Spring MVC design pattern. (More on Spring MVC later)
trainingfulfilmentprocess		: To be used for extending order fulfilment related functionality
traininginitialdata			: To be used for storing essential data that will be imported in the project during initialization
trainingstorefront			: To be used for extending the frontend of the project
trainingtest				: To be used for creating test classes
</pre>

<p>Once modulegen finishes successfully, it will ask you to remove some entries from localextensions.xml and add entries for the newly created extensions. Since we do not need many of the extensions form the default b2c accelerator for this tutorial, we can comment them out from the localextensions.xml in order to significantly reduce our build and server start times.</p>

<p>Go ahead and comment out all the extensions mentioned below from the localextensions.xml file:</p>

<pre>
yacceleratorcockpits
yacceleratorinitialdata
yacceleratorfulfilmentprocess
yacceleratorstorefront
ycommercewebservicestest
electronicsstore
couponbackoffice
permissionswebservices
previewpersonalizationweb
personalizationcmsweb
personalizationsmartedit
personalizationpromotionssmartedit
personalizationsampledataaddon
personalizationpromotions
personalizationpromotionsweb
personalizationservicesbackoffice
personalizationpromotionsbackoffice
personalizationcmsbackoffice
personalizationsearchbackoffice
personalizationsearchsmartedit
personalizationsearchweb
personalizationsearchsamplesaddon
textfieldconfiguratortemplatebackoffice
textfieldconfiguratortemplateaddon
assistedserviceatddtests
promotionengineatddtests
textfieldconfiguratortemplateatddtests
</pre>

<p>A commented out extension will look like this:</p>

<pre>&lt;!-- &lt;extension name='electronicsstore' /&gt; --&gt;</pre>

<div class="alert alert-info" role="alert">
TIP : Note that we have commented out the electronicsstore extension since we will only be working with the apparelstore in this tutorial.
</div>

<p>Now append the extensions that we just created using the modulegen tool to the localextensions.xml file:</p>

<pre>
<code class="language-markup">&lt;extension name="trainingfulfilmentprocess"/&gt;
&lt;extension name="trainingcockpits"/&gt;
&lt;extension name="trainingcore"/&gt;
&lt;extension name="trainingfacades"/&gt;
&lt;extension name="trainingtest"/&gt;
&lt;extension name="traininginitialdata"/&gt;
&lt;extension name="trainingstorefront"/&gt;</code>
</pre>

<div class="alert alert-info" role="alert">
TIP : Since we created these extensions in the default "custom" directory, hybris can see and load them as is. But if you create them in any other location you will have to provide their full path for hybris to be able to find them. There are other ways which you can explore yourselves for loading extensions including autoloading entire directories. These will come handy when you have all your custom extensions being loaded from a git reposotory on your machine.
</div>



<p>In order for our changes to the localextensions.xml to be picked up we need to build the project. Execute the following command to build your project:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>" data-output="2,3">
<code class="language-powershell">setantenv.bat
Setting ant home to: C:\hybris\yayt\hybris\bin\platform\apache-ant-1.9.1
Apache Ant(TM) version 1.9.1 compiled on May 15 2013
ant build</code>
</pre>

<p>This shouldn't take more than a few minutes. Ensure the command exists with a "BUILD SUCCESSFULL" message. Start the server again the same way we did before:</p>

<pre class="command-line language-powershell" data-prompt="C:\hybris\yayt\hybris\bin\platform>" data-output="2,3">
<code class="language-powershell">hybrisserver.bat</code>
</pre>

<p>What has essentially happened now is that the OOTB extensions have been replaced by the new extensions we have created and all essentials files have been copied to our new extensions. So now the location of our apparel store will change from :</p>

<p><a href="https://apparel-uk.local:9002/yacceleratorstorefront/apparel-uk/en/">https://apparel-uk.local:9002/yacceleratorstorefront/apparel-uk/en/</a></p>

<p>to</p>

<p><a href="https://apparel-uk.local:9002/trainingstorefront/en/">https://apparel-uk.local:9002/trainingstorefront/en/</a></p>

<p>Ensure the apparel site is showing up exactly the same way it was before. There might be a chance where the apparel site will not show up anymore using the new trainingstorefront. Initializing the system at this point should solve the problem.</p>

<p>Let's learn an alternate slightly faster method of initializing our system.</p>

<p>Navigate to:</p>

<p><a href="https://apparel-uk.local:9002/hac" target="_blank">https://apparel-uk.local:9002/hac</a></p>

<p>If you get a 404 page, just click on the hybris logo on the page and you will land on the login page for the HAC - "Hybris Administration Console".</p>
<p>All default passwords on hybris for the administrator account are:</p>

<p>username : admin<br>password : nimda</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/extensions/hac_login.png">

<p>Use the above credentials to login to hac. Once again, if you get a 404 page, just click on the hybros logo on the page. If logged in successfully you should see a large heading with the uptime of your hybris server and a few charts displaying the memory, cpu and other resource utilizations.</p>

<p>In the header , navigate to "Platform" -> "Initialization". Wait for the page to load all the extensions you had listed in the localextensions.xml file. Ensure all our training extensions are present in the list. Having all the extensions checked click the "Initialize" button and the server will begin the initialization process.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/extensions/hac_initialize.png">

<p></p>
<div class="alert alert-info">
TIP : Time for another break. The process should take somewhere close to 30 minutes or more depending on your system configuration.
</div>

<p>Once the initialization has successfully completed, the bottom of the page will show a "Continue" link.</p>

<p>Navigate to <a href="https://apparel-uk.local:9002/trainingstorefront/en/" target="_blank">https://apparel-uk.local:9002/trainingstorefront/en/</a> and ensure the apparel storefront is showing up correctly and all functionality is working.</p>

<p>Now that you have added custom extensions to your project, make sure IntelliJ has correctly identified them. Check the project view toolbar on the left side and under the custom module ensure that you see the "training" folder and the seven newly created extensions under it. If you dont, nothing to worry. Click on "File" -> "Close Project" and follow all the steps of importing a new project from chapter 3. This will make sure all the new extensions are picked up by IntelliJ. </p>

<p>Congratulations! You have successfully installed a hybris accelerator and created extensions which we will use in further chapters for modifying the OOTB apparel site.</p>

<div class="alert alert-info">
TIP : If things don't seem to be working properly, follow the magic mantra : stop server -> ant clean all -> start the server -> go to hac -> initialize. As long as there are no mistakes on your parts, the above steps will almost always fix anything that seems to not be working. Beware though, initialization will erase your database and rebuild it with the default sample data provided.
</div>