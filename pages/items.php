<h2>Hybris Item types</h2>

<p>Now that we have both our accelerators and our custom extensions in place lets take a look at some of the internal workings wiring of Hybris.</p>

<p>Hybris internally uses the Hibernate ORM for mapping objects and relations. To understand an ORM in simple terms think of how you would go out building a basic database driven website. You would create a tables for each entity of your application and then create relations between the tables using foreign keys to map the relations between the entities. ORM or Object Relational Mapping gives this job an Object Oriented twist. When using an ORM each entity in your application will be an object that you will define using any structured language(Hibernate uses XML for this). You will then further define relationships between these objects. Once you have defined all your entities and relations, the ORM library will auto-generate all necessary class files for each entity, the relations between them, the tables for each entity and its relation. All database manipulation now gets mapped to objects that you can move around, search, retrieve, save and delete, all without having to write any database queries.</p>

<p>Hybris uses the name "item" for what we would call entities in other relational databases. Lets take a look at how Hibernate is used for defining an entity in Hybris. The OOTB apparel site has a "Product" class that is used for defining the products to be sold on the site. Head over to IntelliJ and press Ctrol+Shift+N to open up the quick search box and look for the file core-items.xml which is present in the extension named "core". Open this file and search for the string code="Product". The file should have only one occurence of this string that looks something like this:</p>

<pre style="height:500px;overflow: auto;">
<code class="language-markup">&lt;itemtype code="Product"
	      extends="GenericItem"
	      jaloclass="de.hybris.platform.jalo.product.Product"
	      autocreate="true"
	      generate="true">
	&lt;deployment table="Products" typecode="1" propertytable="ProductProps"/>
	&lt;attributes>
	    &lt;attribute autocreate="true" qualifier="code" type="java.lang.String" generate="true">
	        &lt;persistence type="property"/>
	        &lt;modifiers read="true" write="true" search="true" initial="true" optional="false" unique="true"/>
	        &lt;custom-properties>
	            &lt;property name="hmcIndexField">
	                &lt;value>"thefield"&lt;/value>
	            &lt;/property>
	        &lt;/custom-properties>
	    &lt;/attribute>
	    &lt;attribute autocreate="true" qualifier="name" type="localized:java.lang.String">
	        &lt;modifiers read="true" write="true" search="true" optional="true"/>
	        &lt;persistence type="property"/>
	        &lt;custom-properties>
	            &lt;property name="hmcIndexField">
	                &lt;value>"thefield"&lt;/value>
	            &lt;/property>
	        &lt;/custom-properties>
	    &lt;/attribute>
	    &lt;attribute autocreate="true" qualifier="unit" type="Unit" generate="true">
	        &lt;persistence type="property"/>
	        &lt;modifiers read="true" write="true" search="true" optional="true"/>
	    &lt;/attribute>
	    &lt;attribute autocreate="true" qualifier="description" type="localized:java.lang.String">
	        &lt;modifiers read="true" write="true" search="true" optional="true"/>
	        &lt;persistence type="property">
	            &lt;columntype database="oracle">
	                &lt;value>CLOB&lt;/value>
	            &lt;/columntype>
	            &lt;columntype database="sap">
	                &lt;value>NCLOB&lt;/value>
	            &lt;/columntype>
	            &lt;columntype>
	                &lt;value>HYBRIS.LONG_STRING&lt;/value>
	            &lt;/columntype>
	        &lt;/persistence>
	        &lt;custom-properties>
	            &lt;property name="hmcIndexField">
	                &lt;value>"thefield"&lt;/value>
	            &lt;/property>
	        &lt;/custom-properties>
	    &lt;/attribute>
	    &lt;attribute autocreate="true" qualifier="thumbnail" type="Media">
	        &lt;persistence type="property"/>
	        &lt;modifiers read="true" write="true" search="true" optional="true"/>
	    &lt;/attribute>
	    &lt;attribute autocreate="true" qualifier="picture" type="Media">
	        &lt;persistence type="property"/>
	        &lt;modifiers read="true" write="true" search="true" optional="true"/>
	    &lt;/attribute>
	&lt;/attributes>
	&lt;indexes>
	    &lt;index name="Product_Code">
	        &lt;key attribute="code"/>
	    &lt;/index>
	&lt;/indexes>
&lt;/itemtype></code>
</pre>

<p>Lets understand how exactly the xml defines the "Product" item.</p>

<p>The "itemtype" tag is used for defining a new item. The "code" attribute decides what the item will be named. The "deployment" tag defines what the database table for this item will be named. Explanation for other attributes in the "deployment" tag can be found on help.hybris.com. Just remember that the "typecode" attribute MUST be unique for each item throughout your hybris project.</p>

<p>The "attributes" tag is used for defining the attributes for our item. Each attribute will be enclosed in an "attribute" tag under "attributes" parent tag. You will notice that the "Product" item has quite a few attributes defined in core-items.xml such as code which is of type string, name which is of type String, description of type String, unit of type Unit which is another item defined in the same xml file, thumbnail of type Media and picture of type Media.</p>

<div class="alert alert-info">
TIP : Note that the attributes name and description are of type "localized" string. This allows us to add multiple values for these attributes depending on the language being used. Further details on this in the impex chapter.
</div>

<p>More custom itemtype definitions can be written by the developer in his own core extension and hybris will merge all of them together and auto generate classes and database tables. We could add more attributes to the "Product" item by using the same xml format and adding the extra attribute in our "C:\hybris\yayt\hybris\bin\custom\training\trainingcore\resources\trainingcore-items.xml" file and hybris would merge all such definitions of the item "Product" into a single definition.</p>

<div class="alert alert-info">
TIP : Remember, we never modify OOTB files. For modifying OOTB items, we will use the trainingcore-items.xml instead of directly changing the core-items.xml file.
</div>

<p>Lets talk about the products being sold on our hybris site, which in our sample apparel site are garments and accesories. Say we are sellng a watch as an accesory. A watch could belong to the itemtype "Product". But, how would we handle the case where we have the same watch but in different colors? Creating two instances of a watch, one for each color would not be a feasible approach. Hybris solves this by defining a "Variant Product" which extends "Product" so that the watch can be defined as a base product of itemtype "Product" and its color, size and other variations can be child classes of type "VariantProduct". Lets take a look at the definition of itemtype "VariantProduct". You will notice that VariantProduct is not defined in core-items.xml. How would we go about looking for it then? Let's use IntelliJ's fantastic search index for this. Head over to IntelliJ and press Ctrl+Shift+F for searching all files of a project. In the search box type 'code="VariantProduct"' and you should see one of the search results from the file catalog-items.xml. Open the file and you'll see that the itemtype "VariantProduct" extends itemtype "Product" indicating the VariantProduct is a child class of type Product. The attributes of VariantProduct are left empty as Hybris expects the developer to add appropriate attributes to it based on the project's requirements.</p>

<pre style="height:500px;overflow: auto;">
<code class="language-markup">&lt;itemtype code="VariantProduct"
          extends="Product"
          jaloclass="de.hybris.platform.variants.jalo.VariantProduct"
          metatype="VariantType"
          autocreate="true"
          generate="true">
	&lt;attributes>
	&lt;!-- changed into relation
	&lt;attribute autocreate="true" qualifier="baseProduct" type="Product">
	  &lt;persistence type="property"/>
	  &lt;modifiers read="true" write="true" search="true" optional="false" initial="true"/>
	&lt;/attribute>
	-->
	&lt;/attributes>
	&lt;!-- created by relation now 
	&lt;indexes>
		&lt;index name="baseIDX" unique="false">
			&lt;key attribute="baseProduct"/>
		&lt;/index>
	&lt;/indexes>
	-->
&lt;/itemtype></code>
</pre>

<p>How is a VariantProduct related to a Product you may ask? This is where hibernate relations come into the picture. A VariantProduct will always have a base product to which it belongs. A base product may have multiple variant products. Hence the relationship between a Product and a VariantProduct is a one to many relationship. This relationship is defined in the catalog-items.xml file as shown in the snippet below:</p>

<pre style="height:500px;overflow: auto;">
<code class="language-markup">&lt;relation code="Product2VariantRelation" autocreate="true" generate="true" localized="false">
	&lt;sourceElement qualifier="baseProduct" type="Product" cardinality="one">
		&lt;modifiers read="true" write="true" search="true" optional="false" initial="true"/>
		&lt;!-- changed into relation 
	    &lt;attribute autocreate="true" qualifier="baseProduct" type="Product">
	      &lt;persistence type="property"/>
	      &lt;modifiers read="true" write="true" search="true" optional="false" initial="true"/>
	    &lt;/attribute>
		 -->
	&lt;/sourceElement>
	&lt;targetElement qualifier="variants" type="VariantProduct" cardinality="many" ordered="false">
		&lt;modifiers read="true" write="true" search="true" optional="true" partof="true"/>
		&lt;custom-properties>
			&lt;property name="ordering.attribute">&lt;value>"code"&lt;/value>&lt;/property>
		&lt;/custom-properties>
		&lt;!-- 
		&lt;attribute autocreate="true" qualifier="variants" type="VPCollection">
	      &lt;persistence type="jalo"/>
	      &lt;modifiers read="true" write="true" search="false" optional="true" partof="true"/>
	    &lt;/attribute>
		 -->
	&lt;/targetElement>
&lt;/relation></code>
</pre>

<p>The sourceElement and targetElement define the relationship entities whereas the cardinalities define the type of relationship which in our case is one for Product and many for VariantProduct. The "qualifier" specifies the attribute name that will be used for accessing the related items i.e the Product will have a list of VariantProduct named "variants" whereas the VariantProduct will have a "baseProduct" of type Product.</p>

<p>Everytime we build our project, these xml files and read and the apprpriate java classes and models are created in their appropriate directory structure. All the attributes we defined in the xml are created in the java classes along with a getter and setter for each of them. When we initialize or update the system, the approprate tables and columns for these models are created in the database.</p>
<p>Lets take a quick look at the generated files for Product and VariantProduct. In IntelliJ search for file ProductModel.java and open it.</p>
<p>You will see that the class ProductModel is actually a child class of ItemModel which serves a the parent class of all itemtypes. If you scroll downwards you will see the name, description, unit, thumbnail and picture attributes that we had seen in core-items.xml being generated here with the comment above them clearly stating that these were picked form the "core" extension. If you remember the relation between Product and VariantProduct we had seen earlier and you will see that the generated code for ProductModel class has an attribute "variants". If we check the getter for attribute variants named "getVariants()" we see that it returns a java Collection of type VariantProductModel which exactly mimics the one to many relationship we had defined. Open the "VariantProductModel.java" file and you will see that it contains a generated attribute named baseProduct whose getter method "getBaseProduct()" returns an object of type ProductModel. This is how relationships between itemtypes are mapped to Objects by the auto-generated code. Hybris also associates the item Product to a ProductCatalogVersion such that each product will belong to a specific Product Catalog. You can see the catalog and catalog version attributes being added to theProduct item in the catalog-items.xml file.</p>

<p>When the b2c accelerator generated the apparel site for us, it also generated some items and attributes for appropriately defining the apparel and accesories to be displayed. Open the file trainingcore-items.xml and you will see three new itemtypes defined. The ApparelyProduct item extends the Product itemtype. ApparelStyleVariantProduct and ApparelSizeVariantProduct items both extend the VariantProduct itemtype. The ApparelStyleVariantProduct item serves to hold variant products with different style and colors of the base product whereas the ApparelSizeVariantProduct serves to hold variant product with different sizes of the base product. Based on the project's requirements a hybris developer may create any kind of itemtype, either completely new or extending an existing itemtype or its sub-type to suit the needs of the project. Below is a class diagram showing the herirarchy of the Product classes we saw above.</p>

<img src="<?=$_ASSETS_ROOT?>/img/pages/items/type_diagram.png" height="500">